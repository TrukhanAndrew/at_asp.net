﻿using NUnit.Framework;
using AutomationDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;

namespace AutomationDemo.Tests
{
    [TestFixture]
    class RemoveLetterPermanentTest: BaseTest
    {
        LoginService loginService = new LoginService();
        MailBoxService mailBoxService = new MailBoxService();
        BasketPageService basketPageService = new BasketPageService();
        CreateAndSendLetterService createAndSendLetterService = new CreateAndSendLetterService();
        IncomingLettersService incomingLettersService = new IncomingLettersService();

        [Test]//тест проверки перманентного удаления из корзины
        public void RemoveLetterPermanentTest_Appeared_True_FromMethod_IsRemovedMailFromBasket()
        {
            loginService.LoginToMailBox(AccountFactory.Account);
            mailBoxService.ClickButtonNewLetter();
            createAndSendLetterService.CreateAndSendLetter(LetterFactory.CreateCorrectLetter());
            mailBoxService.ClickButtonIncomingLetters();
            incomingLettersService.RemoveInABasket();
            mailBoxService.ClickButtonBasket();
            basketPageService.RemoveFromBasket();

            bool value = basketPageService.IsLetterFromLetterFactory();
            Assert.That(value, Is.EqualTo(false));
        }
    }


}
