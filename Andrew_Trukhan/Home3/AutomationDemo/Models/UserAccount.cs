﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Services.Models
{
    public class UserAccount
    {
        public string Login { get; private set; }
        public string Password { get; private set; }
        public string Email { get; set; }

        public UserAccount(string name, string password, string email)
        {
            Login = name;
            Password = password;
            Email = email;
        }
    }
}
