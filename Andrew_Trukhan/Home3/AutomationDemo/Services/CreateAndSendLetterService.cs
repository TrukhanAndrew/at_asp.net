﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;
using AutomationDemo.Pages;

namespace AutomationDemo.Services
{
    public class CreateAndSendLetterService
    {
        private CreateLetterPage createLetterPage = new CreateLetterPage();

        public void CreateAndSendLetter (NewLetter newLetter)
        {
            createLetterPage.FindEmailInput().SendKeys(newLetter.Email);
            createLetterPage.FindThemeInput().SendKeys(newLetter.Theme);
            createLetterPage.FindContentInput().SendKeys(newLetter.Content);
            createLetterPage.FindSubmitButton().Click();
        }

        public string RetrieveErrorOnFailedSendingLetter()
        {
            return new SendLetterFailedPage().ErrorMessage().Text;
        }
    }
}
