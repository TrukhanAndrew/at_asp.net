﻿using NUnit.Framework;
using Moq;
using AT_Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Unit.Tests
{
    [TestFixture]
    public class RepoCalcTests
    {
        [Test]
        public void nextSumTest_2plus3_returns5()
        {
            // arrange
            var repo = new MockRepo();
            repo.Values = new List<int>() { 2, 3 };
            RepoCalc calc = new RepoCalc(repo);
            int expected = 5;

            // act
            int actual = calc.NextSum();

            // assert
            Assert.AreEqual(expected, actual);
        }

        // using moq
        [Test]
        public void nextSumTest_5plus10_returns15()
        {
            var mock = new Mock<IRepo>();
            mock.Setup(repo => repo.NextInts()).Returns(new List<int>() { 5, 10 });

            RepoCalc calc = new RepoCalc(mock.Object);
            int expected = 15;

            int actual = calc.NextSum();

            Assert.AreEqual(expected, actual);
        }
    }
}