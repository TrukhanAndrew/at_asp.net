﻿using AutomationDemo.Framework;
using AutomationDemo.Services.Models;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class OutgoingLettersPage
    {
        private Browser browser = Browser.Instance;
        private static string selectorCSSLetterFromLetterFactory = String.Format("span[title='{0}']", LetterFactory.defaultTheme);

        private static readonly By letterFromLetterFactoryByCss = By.CssSelector("selectorCSSLetterFromLetterFactory");

        public IWebElement FindLetterFromLetterFactory ()
        {
            return browser.FindElement(letterFromLetterFactoryByCss); 
        }
    }
}
