﻿using AutomationDemo.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Pages
{
    class CreateLetterPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By emailInputByCss = By.CssSelector(".mail-Compose-Field-Input input[class='js-suggest-proxy']");
        private static readonly By themeInputByCss = By.CssSelector(".mail-Compose-Field-Input input[name='subj']");
        private static readonly By contentInputByCss = By.CssSelector(".cke_wysiwyg_div div");
        private static readonly By submitButtonByCss = By.CssSelector("button[type='submit'][title='Отправить письмо (Ctrl + Enter)']");

        public IWebElement FindEmailInput()
        {
            return browser.FindElement(emailInputByCss);
        }
        public IWebElement FindThemeInput()
        {
            return browser.FindElement(themeInputByCss); 
        }
        public IWebElement FindContentInput ()
        {
            return browser.FindElement(contentInputByCss); 
        }
        public IWebElement FindSubmitButton ()
        {        
            return browser.FindElement(submitButtonByCss); 
        }
    }
}
