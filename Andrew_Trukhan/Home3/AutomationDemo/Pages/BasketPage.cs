﻿using AutomationDemo.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Services.Models;

namespace AutomationDemo.Pages
{
    class BasketPage
    {
        private Browser browser = Browser.Instance;

        public static readonly string xpathCelectorLetterFromLetterFactory = String.Format(".//div[@class='mail-MessageSnippet-Content']//span[@title='{0}']", LetterFactory.defaultTheme);
        public static readonly string xpathCheckBoxCelector = String.Format(".//div[@class='mail-MessageSnippet-Content']//span[@title='{0}']/ancestor :: div//input[@class='_nb-checkbox-input']", LetterFactory.defaultTheme);

        public static readonly By buttonRemovePermanentByCss = By.CssSelector("div[title='Удалить (Delete)']");//*селектор поиска "кнопки перманентного удаления письма"
        private static readonly By letterFromLetterFactoryByXPath = By.XPath(xpathCelectorLetterFromLetterFactory);
        private static readonly By checkBoxLetterByXPath = By.CssSelector(xpathCheckBoxCelector); //селектор чекбокса найденного файла


        public IWebElement FindLetterFromLetterFactory()
        {
            return browser.FindElement(letterFromLetterFactoryByXPath);
        }
        
        public IWebElement FindCheckBoxLetter()
        {
            return browser.FindElement(checkBoxLetterByXPath);
        }

        public IWebElement FindButtonRemovePermanent()
        {
            return browser.FindElement(buttonRemovePermanentByCss);
        }

    }
}
